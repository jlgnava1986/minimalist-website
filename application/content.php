<?php
	/* Javascript Files */
	include("src/javascript.php"); 
	
	/* Top section */
	include("src/header.php");
			
	/* User-defined functions for content */
	include("src/content-functions.php");
			
	/* Other sections */
	include("src/profile.php");
	include("src/highlights.php");
	include("src/photos.php");
	include("src/contact.php");
	include("src/footer.php");
			
	/* Modals*/
	include("src/modals.php");
?>	