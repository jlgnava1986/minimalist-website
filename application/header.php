<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title>
<?php		
	$page_title = "JLGNava";
	switch($page){
		case "login":
		case "contact":
			$page_section = "Dashboard";
		break;
		default:
			$page_section = "Blogger &amp; Photo Journalist";
	}
	echo pageTitle($page_title, $page_section);	
?>
</title>
<meta name="viewport" content="width=device-width, initial-scale=1">	

<link rel="shortcut icon" href="img/favicon.ico" />
<link rel="stylesheet" media="screen" type="text/css" href="css/bootstrap.min.css" />
<link rel="stylesheet" media="screen" type="text/css" href="css/style.css" />
</head>

<body data-spy="scroll" data-target=".navbar">
	<div class="container-full">