<?php
	require("src/init.php");
	include("src/functions.php");
	include("src/process.php");
	
	/* Header */
	include("header.php");
	
	/* Content*/
	include("content.php");
	
	/* Footer */	
	include("footer.php");
?>