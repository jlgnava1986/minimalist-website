var base = this;
var window_height;
var doc_height;

base.$window = $(window),
base.$document = $(document);

base.$document.ready(function() {
	
	/* Initialized variables*/
	scroll_scrolltop = base.$window.scrollTop();
	window_height = base.$window.height();
	doc_height = base.$document.height();
	window_width = base.$window.width();
	doc_width = base.$document.width();
	
	/* Scrolling */
	var	transparent_size = 10.3,
		background_size = 1.2,
		scroll_profile = $("#profile").offset().top - (window_height/background_size),
		scroll_highlights = $("#highlights").offset().top - (window_height/transparent_size),
		scroll_photos = $("#photo-gallery").offset().top - (window_height/background_size),
		scroll_contact = $("#contact").offset().top - (window_height/transparent_size),
		scroll_top_count = 0,
		scroll_profile_count = 0,
		scroll_highlight_count = 0,
		scroll_gallery_count = 0,
		scroll_contact_count = 0,
		scroll_section = "",
		scroll_scrolltop = "";
		
	var $jlgnava = {
		
		/* Page load*/
		init: function(){
			var $body = $("body"),
				$container = $(".container"),
				$navbar_container = $(".navbar .container"),
				$footer_container = $("#footer .container");
				
			$body.addClass("load");
			$container.css({opacity: 0});	
			$navbar_container.css({opacity: 1});	
			$footer_container.css({opacity: 1});	
		},
		
		/* Navigation slidedown */
		nav_slide: function(){
			var $fixedTop = $(".navbar-fixed-top"),
				$navBar = $(".navbar");
			
			base.$window.scroll(function() {
				profile_length = $navBar.offset().top > scroll_profile && $navBar.offset().top <= scroll_highlights;
				gallery_length = $navBar.offset().top > scroll_photos && $navBar.offset().top <= scroll_contact;
				
				if(profile_length || gallery_length) {
					$fixedTop.addClass("navbar-slide");
				} 
				else{
					$fixedTop.removeClass("navbar-slide");
				}
			});
		},
		
		/* Scrolling */
		scroll_movement: function(){
			var nav = ".navbar #logo a, .nav-top a, ",
				btn = ".scroll-down-btn, .nextbutton, .back-top",
				btn_scroll = nav + btn,
				navbar_height = $(".navbar").outerHeight();
			
			$(btn_scroll).bind('click', function(e) {
				var sectionId=$(this).attr('href'),
					$htmlBody=$('html, body');
				if(sectionId.indexOf('#')!==0)
					return;
				e.preventDefault();
				var $section=$(sectionId);
				$htmlBody.stop().animate({
					scrollTop: $section.offset().top
				}, 1500, "easeInOutExpo");
			});
		
		},
		
		/* Top section */
		top_section: function(){
			var top_count = 0,
				layout = $(".front-cover"),
				container = $("#top-section .container"),
				fade_effect = container.stop(true),
				duration = 500;
		
			layout.css({opacity: 0});	
			fade_effect.animate({opacity: 1}, duration, "easeInOutExpo");
			
			var timeout = setInterval(function(){
				if(top_count < layout.length){
					top_count++;
					title_effect = $(".front-cover.top-item-"+top_count);
					
					/* Title captions */
					if(top_count==2)
						caption_display();
					
					/* Title & button */
					else{
						switch(top_count){
							case 1: max_duration = duration/2;
							break;
							case 3: max_duration = duration*2;
							break;
						}
						title_effect.stop(true).delay(max_duration).animate({opacity: 1}, duration);	
					}
				}
				if(top_count == layout.length){
					clearInterval(timeout);
				}
			}, duration/2, "easeInOutExpo");	
			
			/* Title caption order */
			var caption_display = function(){
				var	caption_count = 0,
					layout = $(".page-captions span"),
					container = $(".page-captions"),
					fade_effect = container.stop(true),
					duration = 500;
				
				layout.css({opacity: 0});	
				fade_effect.animate({opacity: 1}, duration, "easeInOutExpo");
				var timeout = setInterval(function(){
					if(caption_count < layout.length){
						caption_count++;
						caption_effect = $(".page-captions span.caption-attribute-"+caption_count);
						caption_effect.stop(true).animate({opacity: 1}, duration);	
					}
					if(caption_count == layout.length){
						clearInterval(timeout);
					}
				}, duration/2, "easeInOutExpo");
			}
		},
		
		other_sections: function(){
			
			base.$window.scroll(function(){
			
				scroll_scrolltop = base.$window.scrollTop();
				
				if(scroll_scrolltop >= scroll_profile){
					scroll_section = "profile";
				}
				if(scroll_scrolltop >= scroll_highlights){
					scroll_section = "highlights";
				}
				if(scroll_scrolltop >= scroll_photos){
					scroll_section = "photo-gallery";
				}
				if(scroll_scrolltop >= scroll_contact){
					scroll_section = "contact";
				}
				
				/* Profile section */
				if(scroll_section == "profile"){
					if(scroll_profile_count == 0){				
						var profile_count = 0,
							layout = $(".profile-layout"),
							container = $("#profile .container"),
							fade_effect = container.stop(true),
							duration = 500,
							max_duration = duration/2,
							nxtbutton = $("#profile .nextbutton.nobg");
								
						layout.css({opacity: 0});	
						nxtbutton.addClass("light-section");
						fade_effect.animate({opacity: 1}, duration, "easeInOutExpo");
						
						var timeout = setInterval(function(){
							if(profile_count < layout.length){
								profile_count++;
								profile_list = $(".profile-layout.item-"+profile_count);
								profile_list.stop(true).delay(max_duration).animate({opacity: 1}, duration);
							}
							if(profile_count == layout.length){
								clearInterval(timeout);
							}
						}, duration/2, "easeInOutExpo");	
						scroll_profile_count = 1;
					}
				}
				
				/* Highlights section */
				if(scroll_section == "highlights"){
					if(scroll_highlight_count == 0){	
						var highlights_count = 0,
							layout = $(".highlights-layout"),
							container = $("#highlights .container"),
							fade_effect = container.stop(true),
							duration = 500;
							nxtbutton = $("#highlights .nextbutton.nobg");
						
						layout.css({opacity: 0});	
						nxtbutton.addClass("dark-section");
						fade_effect.animate({opacity: 1}, duration, "easeInOutExpo");
						
						var timeout = setInterval(function(){
							if(highlights_count < layout.length){
								highlights_count++;
								highlights_list = $(".highlights-layout.item-"+highlights_count);
								
								/* Highlights set */
								if(highlights_count==2)
									highlight_set();
								
								/* Title & button */
								else{
									switch(highlights_count){
										case 1: max_duration = duration/2;
										break;
										case 3: max_duration = duration*2;
										break;
									}
									highlights_list.stop(true).delay(max_duration).animate({opacity: 1}, duration);	
								}
							}
							if(highlights_count == layout.length){
								clearInterval(timeout);
							}
						}, duration/2, "easeInOutExpo");
						
						/* Highlights set layout*/
						var highlight_set = function(){
							var set_count = 0, 
								layout = $(".highlight-set-item"),
								container = $(".highlights-layout.item-2"),
								fade_effect = container.stop(true),
								duration = 500;
							
							layout.css({opacity: 0});	
							fade_effect.animate({opacity: 1}, duration, "easeInOutExpo");
							var timeout = setInterval(function(){
								if(set_count < layout.length){
									set_count++;
									set_effect = $(".highlight-set-item.highlight-set-"+set_count);
									set_effect.stop(true).animate({opacity: 1}, duration);	
								}
								if(set_count == layout.length){
									clearInterval(timeout);
								}
							}, duration/2, "easeInOutExpo");
						}
						
						scroll_highlight_count = 1;
						
					}
				}
				
				/* Photo gallery section */
				if(scroll_section == "photo-gallery"){
					if(scroll_gallery_count == 0){				
						var gallery_count = 0,
							layout = $(".photo-gallery-layout"),
							container = $("#photo-gallery .container"),
							fade_effect = container.stop(true),
							duration = 500;
							nxtbutton = $("#photo-gallery .nextbutton.nobg");
						
						layout.css({opacity: 0});	
						nxtbutton.addClass("light-section");
						fade_effect.animate({opacity: 1}, duration, "easeInOutExpo");
						
						var timeout = setInterval(function(){
							if(gallery_count < layout.length){
								gallery_count++;
								gallery_list = $(".photo-gallery-layout.item-"+gallery_count);
								
								/* Highlights set */
								if(gallery_count==2)
									photo_gallery_set();
								
								/* Title & button */
								else{
									switch(gallery_count){
										case 1: max_duration = duration/2;
										break;
										case 3: max_duration = duration*2;
										break;
									}
									gallery_list.stop(true).delay(max_duration).animate({opacity: 1}, duration);	
								}
							}
							if(gallery_count == layout.length){
								clearInterval(timeout);
							}
						}, duration/2, "easeInOutExpo");
						
						var photo_gallery_set = function(){
							var set_count = 0, 
								layout = $(".photo-gallery-item"),
								container = $(".photo-gallery-layout.item-2"),
								fade_effect = container.stop(true),
								duration = 500;
							
							layout.css({opacity: 0});	
							fade_effect.animate({opacity: 1}, duration, "easeInOutExpo");
							var timeout = setInterval(function(){
								if(set_count < layout.length){
									set_count++;
									set_effect = $(".photo-gallery-item.photo-gallery-"+set_count);
									set_effect.stop(true).animate({opacity: 1}, duration);	
								}
								if(set_count == layout.length){
									clearInterval(timeout);
								}
							}, duration/2, "easeInOutExpo");
						}
						
						scroll_gallery_count = 1;
					}
				}
				
				/* Contact section */
				if(scroll_section == "contact"){
					if(scroll_contact_count == 0){				
						var contact_count = 0,
							layout = $(".contact-form"),
							container = $("#contact .container"),
							fade_effect = container.stop(true),
							duration = 500,
							max_duration = duration/2;
						
						layout.css({opacity: 0});	
						fade_effect.animate({opacity: 1}, duration, "easeInOutExpo");
						
						var timeout = setInterval(function(){
							if(contact_count < layout.length){
								contact_count++;
								contact_form = $(".contact-form.item-"+contact_count);
								
								/* Contact form */
								if(contact_count==2)
									contact_form_layout();
									
								/* Contact details */
								else if(contact_count==3)
									contact_details();
									
								/* Title */
								else
									contact_form.stop(true).delay(max_duration).animate({opacity: 1}, duration);
							}
							if(contact_count == layout.length){
								clearInterval(timeout);
							}
						}, duration/2, "easeInOutExpo");
						
						var contact_form_layout = function(){
							var group_count = 0, 
								layout = $(".form-group"),
								container = $(".contact-form.item-2"),
								fade_effect = container.stop(true),
								duration = 500,
								max_duration = duration/2;
							
							layout.css({opacity: 0});	
							fade_effect.animate({opacity: 1}, duration, "easeInOutExpo");
							var timeout = setInterval(function(){
								if(group_count < layout.length){
									group_count++;
									group_effect = $(".form-group.form-group-"+group_count);
									group_effect.stop(true).delay(max_duration).animate({opacity: 1}, duration);	
								}
								if(group_count == layout.length){
									clearInterval(timeout);
								}
							}, duration/2, "easeInOutExpo");
						}
						
						var contact_details = function(){
							var details_count = 0, 
								layout = $(".details-layout"),
								container = $(".contact-form.item-3"),
								fade_effect = container.stop(true),
								duration = 500,
								max_duration = duration*2.5;
							
							layout.css({opacity: 0});	
							fade_effect.animate({opacity: 1}, duration, "easeInOutExpo");
							var timeout = setInterval(function(){
								if(details_count < layout.length){
									details_count++;
									details_effect = $(".details-layout.details-item-"+details_count);
									details_effect.stop(true).delay(max_duration).animate({opacity: 1}, duration);	
								}
								if(details_count == layout.length){
									clearInterval(timeout);
								}
							}, duration/2, "easeInOutExpo");
						}
						
						scroll_contact_count = 1;
					}
				}
				
			});
			
			
		},
		
		/* Validating contact form */
		check_form: function(){
			
			/* Submit values */
			$("#submit").bind("click", function(e){
				var name = $("#fullname").val(),
					email = $("#email").val(),
					subject = $("#subject").val(),
					message = $("#message").val(),
					required = name == "" || email == "" || subject == "" || message == "";
					
					if(required){
						
						/* Checking fields */
						$("#contact-form").bootstrapValidator({
							excluded: ':disabled',
							message: "Please fill out the following forms.",
							feedbackIcons:{
								valid: null,
								invalid: null,
								validating: null
							},
							fields:{
								name:{
									validators:{ 
										stringLength:{ 
											min: 2,
										},
										notEmpty:{ 
											message: "The name is required."	
										}
									}
								},
								email:{
									validators:{ 
										emailAddress:{ 
											message: "Invalid email address."
										},
										notEmpty:{ 
											message: "The e-mail address is required."
										}
									}
								},
								subject:{
									validators:{ 
										stringLength:{ 
											min: 2,
										},
										notEmpty:{
											message: "The subject is required."	
										}
									}
								}
							}
						});
						
					}
					else{
						
						/* Submitting data successfully*/
						$.ajax({
							url: "ajax-mail", /* Refer to the .htaccess file */
							method: "POST",
							data:{
								name: name,
								email: email,
								subject: subject,
								message: message	
							},
							success: function(data){
								$("#contact-form").trigger("reset");
								$(".success-msg").fadeIn().html(data);
								setTimeout(function(){
									$(".success-msg").fadeOut("slow");
								}, 2000);
							}
						});
						e.preventDefault();
						
					}
				
			});
			
		},
		
		open_modal: function(){
			
			var $openModal = $(".highlight-open-modal"),
				$closeModal = $(".close-modal");
			
			/* Opening of a modal */
			$openModal.on("click", function(){
					
				var modal_count = 0,
					layout = $(".modal-layout"),
					container = $(".modal-content"),
					fade_effect = container.stop(true),
					duration = 600;
							max_duration = duration/2;
				
				layout.css({opacity: 0});	
				fade_effect.animate({opacity: 1}, duration, "easeInOutExpo");
				
				var timeout = setInterval(function(){
					if(modal_count < layout.length){
						modal_count++;
						modal_layout_effect = $(".modal-layout.modal-item-"+modal_count);
						if(modal_count==2)
							animation_chain();
						modal_layout_effect.stop(true).delay(max_duration).animate({opacity: 1}, duration);
					}
					if(modal_count == layout.length){
						clearInterval(timeout);
					}
				}, duration/2, "easeInOutExpo");
				
			});
			
			/* Closing of the modal */
			$closeModal.on("click", function(){
				var $layout = $(".modal-layout"),
					$container = $(".modal-content"),
					$animation_chain = $(".animation-chain"),
					animationName = $animation_chain.data("animation");
					animationEnd = 'webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend';

				/* Removes the animation and display of modal content */
				$layout.css({opacity: 0});
				$container.css({opacity: 0});
				$animation_chain.children().removeClass("animated").one(animationEnd, function(){
					$(this).removeClass(animationName);
				});
				$animation_chain.children(":not(:last-child)").removeAttr("class");
				$animation_chain.children().removeAttr("style");
			});
			
			/* Triggers animation of the content */
			var animation_chain = function(){
				function onScrollHandler(){
					var $animation_chain = $(".animation-chain");
					$animation_chain.each(function(index,el){
						var $el = $(el),
							animationName = $el.data("animation");
						$el.animateCssChain(animationName);
					});
				}
				window.requestAnimationFrame(onScrollHandler); 
			}
			
			/* Extended script of animation to the content */
			$.fn.extend({
				
				/* Removes the animation data of the class attribute */
				animateCss: function(animationName){
					var animationEnd='webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend';
					$(this).addClass('animated '+ animationName).one(animationEnd,function(){
						$(this).removeClass(animationName);
					});
				},
				
				/* Sets the timeframe for the animation */
				animateCssChain: function(animationName,delay){
					if(delay===undefined||delay===null||delay==='')
						delay=0.5;
							
					$(this).children().each(function(index,el){
						index = index+1;
						var $el=$(el);
						if($el.hasClass('animated'))
							return true;
						$el.css({
							'-webkit-animation-delay':delay*index+'s',
							'animation-delay':delay*index+'s'
						}).animateCss(animationName);
					});
				}
			});
		},
		
		toggle_menu: function(){
		
			var $toggle_sidebar = $(".navbar-toggle");
				
			$toggle_sidebar.click(function(){
				$(this).toggleClass("open");
			});
			
		}
	
	}
	
	/* Functions*/
	$jlgnava.top_section();
	$jlgnava.other_sections();
	
	/* AJAX & Bootstrap Validator*/
	$jlgnava.check_form();
	
	/* Modal content*/
	$jlgnava.open_modal();
	
	/* Animations */
	$jlgnava.init();
	$jlgnava.nav_slide();
	$jlgnava.scroll_movement();
	
	/* Mobile usage */
	$jlgnava.toggle_menu();
	
});
