// JavaScript Document
(function($){"use strict";
	var $body=$('body'),
		$htmlBody=$('html, body'),
		isFrontPage=$body.hasClass('home');
		
		$('.img-as-bg').each(function(index,el){
			var $img=$(el);
				$img.parent().css('background-image','url("'+ $img.attr('src')+'")');
				$img.remove();
		});
		
		var $nav=$('nav'),
			$menu=$('nav ul.menu'),
			$menuItems=$menu.find('li'),
			$openMenu=$('.open-menu'),
			openMenuLock=false,
			
		$scrollUp=$('a.scroll-up');
		$menu.find('li').each(function(index,el){
			$(this).css('transition-delay',index*0.05+'s');
		});
		
		$menuItems.find('a').on('click',function(e){
			var sectionId=$(this).attr('href');
			if(sectionId.indexOf('#')!==0)
				return;
			e.preventDefault();
			var $section=$(sectionId);
			$htmlBody.animate({
				scrollTop:$section.offset().top
			},300);	
		});
		
		function getCurrentSection(){
			var currentSection=null;
			$('section').each(function(index,el){
				var $this=$(this),
					rangeTop=$this.offset().top- $win.height()/ 2,
					rangeBottom=rangeTop+ $this.outerHeight();
				
				if(rangeTop<$win.scrollTop()&&$win.scrollTop()<rangeBottom){
					currentSection=el;
					return false;
				};
			});
			return currentSection;
		}
		
		$openMenu.on('click',function(){
			if(openMenuLock)
				return;
			openMenuLock=true;
			if($nav.hasClass('opened')){
				$menu.removeClass('show-menu-items');
				setTimeout(function(){
					$nav.removeClass('opened');
					openMenuLock=false;
					$menuItems.find('a').removeClass('current');
				},300);
			}
			else{
				var currentSectionId=$(getCurrentSection()).attr('id');
				$menuItems.each(function(index,el){
					var $this=$(this),
						$a=$this.find('a'),
						href=$a.attr('href');
					
					if(href==='#'+ currentSectionId){
						$a.addClass('current');
					}
				});
				$nav.addClass('opened');
				setTimeout(function(){
					$menu.addClass('show-menu-items');
					openMenuLock=false;
				},300);
			}
		});
		
		$menu.find('a').on('click',function(e){
			$openMenu.click();
		});
		
		$nav.on('click',function(e){
			var tagName=$(e.target).parent().get(0).tagName;
			if(tagName!=='LI'&&tagName!=='NAV'){
				$openMenu.click();
			}
		});
		$scrollUp.on('click',function(e){
			$htmlBody.animate({
				scrollTop:0
			},1000);
		});
			
		var $titles=$('.titles'),
			$heroSlides=$('.hero-slides');
		
		if(isFrontPage&&$.fn.textillate!==undefined){
			$titles.textillate({
				selector:'.texts',
				loop:$('.titles li').length>1,
				minDisplayTime:5000,
				initialDelay:1000,
				autoStart:true,
				inEffects:[],
				outEffects:[],
				in:{
					effect:'fadeInUp',
					delayScale:1.5,
					delay:50,
					sync:true,
					reverse:false,
					callback:function(){}
				},
				out:{
					effect:'fadeOutUp',
					delayScale:1.5,
					delay:50,
					sync:true,
					shuffle:false,
					reverse:false,
					callback:function(){}
				},
				callback:function(){},
				type:'word'}
			);
			$titles.on('outAnimationBegin.tlt',function(e){
				$heroSlides.trigger('next.owl.carousel');
			});
			$heroSlides.owlCarousel({
				loop:true,
				margin:0,
				nav:false,
				items:1,
				dots:false,
				smartSpeed:1800,
				mouseDrag:false,
				touchDrag:false,
			});
			$titles.on('start.tlt',function(){
				$('.down-arrow').addClass('play');
			});
			$('.scroll-down-indicator').on('click',function(e){
				$htmlBody.animate({
					'scrollTop':$win.height()
				},800);
			});
		}
			
		var $grid=$('.grid');
		if(isFrontPage&&$.fn.imagesLoaded!==undefined){
			$grid.imagesLoaded(function(){
				$grid.isotope({
					itemSelector:'.grid-item',
					layoutMode:'masonry'
				});
			});
		}
		
		var $portfolioCats=$('.portfolio-cats a');
		$portfolioCats.on('click',function(e){
			e.preventDefault();
			var cat=$(this).data('cat');
			if(cat!=='*'){
				cat='.'+ cat;
			}
			$grid.isotope({
				filter:cat
			});
		});
		var $portfolioItems=$('#portfolio .grid-item'),
			$portfolioModal=$('.portfolio-modal'),
			$portfolioModalNavPrev=$('.portfolio-modal .modal-nav-prev'),
			$portfolioModalNavNext=$('.portfolio-modal .modal-nav-next'),
			$portfolioModalNavClose=$('.portfolio-modal .modal-nav-close'),
			$portfolioOverlay=$('.portfolio-overlay'),
			$portfolioOpenModal=$('.portfolio-open-modal');
			
		$portfolioModalNavPrev.on('click',function(e){
			e.preventDefault();
			$portfolioItems.filter('.current').prev().trigger('click');
		});
		$portfolioModalNavNext.on('click',function(e){
			e.preventDefault();
			$portfolioItems.filter('.current').next().trigger('click');
		});
		$portfolioModalNavClose.on('click',function(e){
			e.preventDefault();
			$portfolioOverlay.trigger('click');
		});
		$portfolioOpenModal.on('click',function(e){
			e.preventDefault();
			$(this).parents('.grid-item').trigger('click');
		});
		function openPortfolioModal(){
			setTimeout(function(){
				$portfolioModal.addClass('opened');
				$portfolioOverlay.addClass('loaded');
			},300);
		}
		$portfolioItems.on('click',function(e){
			if(e.target.tagName.toLowerCase()!=='img'&&!$(e.target).hasClass('grid-item'))
				return;
			var $this=$(this),
				$info=$this.find('.portfolio-info'),
				$left=$portfolioModal.find('.left'),
				$right=$portfolioModal.find('.right'),
				$imageList=$this.find('ul.image-list'),
				$video=$this.find('ul.video');
				
			$this.addClass('current').siblings().removeClass('current');
			$portfolioModalNavPrev.parent().toggleClass('enabled',$this.prev().length>0);
			$portfolioModalNavNext.parent().toggleClass('enabled',$this.next().length>0);
			$left.empty().append($info.clone());
			$right.empty();
			
			if($imageList.length>0){
				var $carousel=$('<div />').addClass('owl-carousel owl-theme');
				$imageList.find('img').each(function(index,el){
					var $img=$(el).clone();
					$img.attr('src',$img.data('src'));
					if($img.hasClass('img-vertical')){
						$img.css('max-height',$win.innerHeight()-240);
					}
					$('<div />').addClass('item').append($img).appendTo($carousel);
				});
				$right.append($carousel);
				$carousel.imagesLoaded(function(){
					$carousel.owlCarousel({
						loop:true,
						margin:0,
						nav:false,
						items:1,
						autoHeight:true,
						dots:true,
					});
					openPortfolioModal();
				});
			}
			if($video.length>0){
				var $iframe=$video.find('iframe'),
					src=$iframe.data('src'),
					$wideScreen=$('<div />').addClass('wide-screen');
					
				if(src.indexOf('youtube')!==-1){
					var srcSplit=src.split('?'),
					srcMain=null,
					srcPure=null;
					if(srcSplit.length>0){
						srcMain=srcSplit[0];
						srcPure=srcMain.split('/');
						srcPure=srcPure.pop();
						var $thumbnail=$('<a />').attr({'href':'#'}).append($('<img/>').
										attr({'src':'http://i.ytimg.com/vi/'+ srcPure+'/maxresdefault.jpg'}));
						$wideScreen.append($thumbnail);
						$wideScreen.imagesLoaded(function(){
							$right.append($wideScreen);
							openPortfolioModal();
						});
						$thumbnail.on('click',function(e){
							e.preventDefault();
							src=src+'&autoplay=1';
							$wideScreen.empty().append($iframe.clone().attr({'src':src}));
						});
					}
				}
				else{
					$wideScreen.append($iframe.clone().attr({
						'src':src}
						).on('load',function(){
							openPortfolioModal();
						})
					);
					$right.append($wideScreen);
				}
			}
				
			$portfolioOverlay.css('display','flex');
			setTimeout(function(){
				$portfolioOverlay.addClass('opened');
			},100);
		});
	
		$portfolioOverlay.on('click',function(e){
			if(!$(e.target).hasClass('portfolio-overlay'))
				return;
			$portfolioModal.find('.right').empty();
			$portfolioModal.removeClass('opened');
			setTimeout(function(){
				$portfolioOverlay.removeClass('opened');
				setTimeout(function(){
					$portfolioOverlay.hide();
					$portfolioOverlay.removeClass('loaded');
				},300);
			},300);
		});
		
		$('#testimonials>.owl-carousel').owlCarousel({
			loop:true,
			margin:0,
			nav:false,
			items:1,
			autoHeight:true,
			dots:true,
			autoplay:true,
			autoplayTimeout:8000,
			autoplayHoverPause:true,
			smartSpeed:600,
		});
		
		var $procesLines=$('svg.process-line>path'),
			$win=$(window),
			winHeight=$win.height(),
			previousWinScrollTop=0;
			
		function onScrollHandler(){
			var winScrollTop=$win.scrollTop();
			$procesLines.css('stroke-dashoffset',winScrollTop/5);
			if(winScrollTop- previousWinScrollTop>0){
				$('.animation-chain').each(function(index,el){
					var $el=$(el);
					if(winScrollTop>$el.offset().top- winHeight/4*3){
						var animationName=$el.data('animation');
						if(animationName===undefined||animationName===''){
							animationName='fadeInUp';
						}
						$el.animateCssChain(animationName);
					}
				});
			}
			previousWinScrollTop=winScrollTop;
			window.requestAnimationFrame(onScrollHandler);
		}
		window.requestAnimationFrame(onScrollHandler);
		$.fn.extend({
			animateCss:function(animationName){
				var animationEnd='webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend';
				$(this).addClass('animated '+ animationName).one(animationEnd,function(){
					$(this).removeClass(animationName);
				});
			},
			animateCssChain:function(animationName,delay){
				if(delay===undefined||delay===null||delay==='')
					delay=0.1;
				$(this).children().each(function(index,el){
					var $el=$(el);
					if($el.hasClass('animated'))
						return true;
					$el.css({
						'-webkit-animation-delay':delay*index+'s',
						'animation-delay':delay*index+'s'
					}).animateCss(animationName);
				});
			}
		});
		
		var $contactFormInputs=$('.contact-main, form').find('input, textarea');$contactFormInputs.on('focus, blur',function(e){var $this=$(this);if(this.value!==''){$this.addClass('focused');}else{$this.removeClass('focused');}});var $mapViewSwitch=$('a.map-view-switch'),$mapCanvas=$('.map-canvas'),$contactMain=$('.contact-main');if($mapCanvas.length>0){$mapCanvas.googleMaps({styled:false,disableDefaultUI:false,latitude:$mapCanvas.data('lat'),longitude:$mapCanvas.data('long'),zoom:15,});}
$mapViewSwitch.on('click',function(e){e.preventDefault();if($mapCanvas.hasClass('opened')){$mapCanvas.removeClass('opened');$contactMain.removeClass('closed');$mapCanvas.data('plugin_googleMaps').removeMarker();}else{$mapCanvas.addClass('opened');$contactMain.addClass('closed');$mapCanvas.data('plugin_googleMaps').addMarker();}});$('.wpcf7-form').find('br').remove();$('.wpcf7-form-control-wrap').each(function(index,el){var $el=$(el);$el.siblings('label, svg').appendTo($el);});})(jQuery);window.mobileAndTabletcheck=function(){var check=false;(function(a){if(/(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|iris|kindle|lge |maemo|midp|mmp|mobile.+firefox|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows ce|xda|xiino|android|ipad|playbook|silk/i.test(a)||/1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i.test(a.substr(0,4)))check=true})(navigator.userAgent||navigator.vendor||window.opera);return check;}
if(!mobileAndTabletcheck()&&typeof(skrollr)!='undefined'){var s=skrollr.init({forceHeight:false});}