<?php
	include("src/functions.php");
	
	/* Database Connection */
	$db_host = "localhost";
	$db_user = "root";
	$db_password = "";
	
	$conn = mysql_connect($db_host, $db_user, $db_password);
	if(!$conn){
		die("<div>Failed to connect to MySQL: ".mysql_error())."</div>";
	}
	$db_create = "CREATE DATABASE jlgnava";
	if(mysql_query($db_create, $conn)){
		$sql_msg = "<div>Database 'jlgnava' has been created successfully.</div>";
		echo $sql_msg;
	}
	else{
		echo "<div>Error: ".mysql_error()."</div>";
	}
	
	/* Create Database Tables */
	$sql = mysql_select_db("jlgnava", $conn);
	$sql_contact = "CREATE TABLE contact (
		id int not null auto_increment, 
		primary key(id),
		name varchar(50) not null,
		email varchar(50) not null,
		subject varchar(200) not null,
		message longtext not null,
		joined datetime not null
	)";
	if(mysql_query($sql_contact, $conn)){
		$sql_msg = "<div>Table 'contact' has been created successfully.</div>";
		echo $sql_msg;
	}
	else{
		echo "<div>Error: ".mysql_error()."</div>";
	}
		
	mysql_close($conn);
	
?>