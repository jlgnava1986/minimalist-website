<?php
	/* Database configuration */
	$GLOBALS["config"] = array(
		"mysql" => array(
			"host" => "localhost",
			"username" => "root",
			"password" => "",
			"db" => "jlgnava"
		)
	);
	
	/* Using the escape function */
	function escape($string){
		return htmlentities($string, ENT_QUOTES, "UTF-8");
	}
	
	/* List of database classes */
	include("classes.php");
	
	/* Check and submit values inside the form */
	if(Input::exists()){
		
		/* Validate each field*/
		$validate = new Validate();
		$validation = $validate->check($_POST, array(
			"name" => array("required" => true, "min" => 2, "max" => 50),
			"email" => array("required" => true, "min" => 3, "max" => 50),
			"subject" => array("required" => true, "min" => 2, "max" => 50)
		));
		
		/* Submission of data */
		if($validation->passed()){
			$guest = new Guest();
			$name = escape(Input::get("name"));
			$email = Input::get("email");
			$subject = Input::get("subject");
			$message = Input::get("message");
			
			try{
				$guest->create(array( 
					"name" => $name,
					"email" => $email,
					"subject" => $subject,
					"message" => $message,
					"joined" => date("Y-m-d H:i:s")
				));
				
				/* E-mail inbox */
				function mail_inbox(){
					global $name, $email, $message;
					$greeting = "Hello JLGNava, ";
					$inbox_msg = "$name has already sent you a feedback about your website. Here's the message: ";
					$css = "<div style='color:#555; font-style:italic; line-height: 1.2; margin-top: 5px;'>";
					$feedback = $css.$message."</div>";
					$closing = "Thank you! ";
					
					$body_mail = array($greeting, $inbox_msg, $feedback, $closing);
					$mail_content = "";
					
					foreach($body_mail as $part_mail){
						switch($part_mail){
							case $greeting:
								$mail_content .= "$part_mail <br><br>";
							break;
							default:
								$mail_content .= "$part_mail <br>";
							break;	
						}
					}
					
					return $mail_content;
				}
				
				$to = "mantaraymarc0714@gmail.com";
				$headers = "From: $email \r\n";
				$headers .= 'MIME-Version: 1.0' . "\n"; 
				$headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";  
				$body = mail_inbox();
				$success = mail($to, $subject, $body, $headers);
				if($success==true)
					echo "Feedback sent successfully!";
				else	
					echo "Error sending feedback";
				
			}
			catch(Exception $e){
				die($e->getMessage());
			}
			
		}
		else{
			foreach($validation->errors() as $error){
				if(!empty($error))
					$note = "Please fill out the following fields.";
			}
		}
	}
?>