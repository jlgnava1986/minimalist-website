<?php
/* Profile description*/

/* First paragraph */
$topDesc = array(
	"Joel, as his friends fondly call him, may often describe himself as a savvy, talented, and Christ-loving ",
	"techie. Apart from his computer expertise, he also loves to sing occasionally with his close friends in ",
	"numerous gigs. A devout Catholic, he serves actively for various religious communities in Metro Manila, ",
	"stemming from his membership role in the youth ministry at Christ the King Parish in Greenmeadows, ",
	"Quezon City."
);

/* Middle paragraph */
$middleDesc = array(
	"The eldest of four siblings in the family, Joel spent most of his life on adversity. ",
	"Since his younger years, he was diagnosed with a congenital cataract on his right eye and high ",
	"functioning autism, both of which may result in him being discriminated and misjudged by the public. ",
	"Nevertheless, he was able to overcome them by excelling in academics and collaborating with others in ",
	"projects to attain his lifetime goals. Thus, Joel’s act of perseverance and humility in every part of ",
	"his education helped him obtain a bachelor’s degree in information technology upon his graduation from ",
	"Asia Pacific College in 2010."
);	

/* Concluding paragraph */
$conclusion = array(
	"Above all things, Joel remains steadfast and truthful to himself and to others despite of his constant ",
	"lifetime struggles. He may be known for his stern and studious manner, but he sincerely enjoys meeting ",
	"new people and befriend them. He also loves to explore, seek new opportunities, and inspire the youth ",
	"through his professional IT skills, musical talent, and ministry service."
);


/* Experience and education */
$exp_educ = array(
	"One Voice Solutions"=>array("Systems Administration Trainee", "June 2017 – present"),
	"Informatics SM Megamall"=>array("Specialist Course in Javascript for Web", "April – May 2017"),
	"Cosmonaut Institute of Information Technology"=>array(
		"Specialist Course in Web Programming", "October 2016 – January 2017"),
	"Integrated Open Source Solutions"=>array("Network Administrator", "June 2010 – January 2015"),
	"Asia Pacific College"=>array(
		"Bachelor's Degree in Computer Science<br>Major in Computer Networks", 
		"June 2006 – June 2010"),
	"Community of Learners Foundation"=>array("Primary & Secondary Education", "June 1995 – March 2006")
);

$ministry_list = array(
	"feast"=>array("The Feast (Valle Verde)", "Servant, Media Ministry", "January 2015 – present"),
	"ctk-1"=>array("Christ the King Parish Greenmeadows", "Lector & Commentator", "August 2014 – August 2016"),
	"sfc"=>array("Singles for Christ (Makati Chapter)", "Member", "January 2014 – January 2015"),
	"ctk-2"=>array("Christ the King Parish Greenmeadows", "Core Officer, Youth Ministry", "October 2011 – December 2013"),
	"apc"=>array("Asia Pacific College", "Core Member & Co-founder, Campus Ministry", "September 2011 – January 2015"),
	"prex"=>array("PREx Youth", "Batch 8, Core Member", "October 2007 – present")
);

$profile_set = array(
	"Name"=>"Jose Luis Garrucho Nava",
	"Nickname"=>"Joel",
	"Birthday"=>"January 6, 1986",
	"Zodiac sign"=>"Capricorn",
	"Birthplace"=>"San Juan, Metro Manila",
	"Hobbies"=>"Singing, writing, reading books, photography",
	"3 words to describe you"=>"Soft-spoken, studious, adventurous",
	"Blog name"=>"JLGNava",
	"How long have you been blogging?"=>"Since December 2013"
);
$favorite_set = array(
	"Color"=>"Blue",
	"Food"=>"Japanese, Italian, American, Filipino",
	"Book/novel"=>"\"Spell Out Your Love\" by Obet Cabrillas",
	"Celebrity crush"=>"Pamu Pamorada",
	"Movie genre"=>"Comedy",
	"TV Show"=>"Any show related to sports",
	"TV Channel"=>"FOX Sports",
	"Sports"=>"Basketball, Football",
	"Band/music artist"=>"Erik Santos",
	"Destination"=>"Cebu",
	"Role model"=>"Martin Luther King",
	"Lifetime ambition"=>"Travel to Staples Center in Los Angeles"
);

?>