<?php
/* General configuration*/
class Config{
	public static function get($path = null){
		if($path){
			$config = $GLOBALS["config"];
			$path = explode("/", $path);
			foreach($path as $bit){
				if(isset($config[$bit])){
					$config = $config[$bit];
				}
			}			
			return $config;
		}
		return false;
	}
}

/* Database setup*/
class DB{
	private static $_instance = null;
	private $_pdo, $_query, $_error = false, $_results, $_count = 0;
	
	private function __construct(){
		try{
			$this->_pdo = new PDO("mysql:host=".Config::get("mysql/host").";dbname=".Config::get("mysql/db"), Config::get("mysql/username"), Config::get("mysql/password"));
		}
		catch(PDOException $e){
			die($e->getMessage());
		}
	}
	
	public static function getInstance(){
		if(!isset(self::$_instance)){
			self::$_instance = new DB();
		}
		return self::$_instance;
	}
	
	public function query($sql, $params = array()){
		$this->_error = false;
		if($this->_query = $this->_pdo->prepare($sql)){
			$i = 1;
			if(count($params)){
				foreach($params as $param){
					$this->_query->bindValue($i, $param);
					$i++;
				}
			}
			
			if($this->_query->execute()){
				$this->_results = $this->_query->fetchAll(PDO::FETCH_OBJ);
				$this->_count = $this->_query->rowCount();
			}
			else{
				$this->_error = true;
			}
		}
		return $this;
	}
	
	public function action($action, $table, $where = array()){
		if(count($where)==3){
			$operators = array("=", ">", "<", ">=","<=", "<>");
			$field = $where[0];
			$operator = $where[1];
			$value = $where[2];
			
			if(in_array($operator, $operators)){
				$sql = "{$action} from {$table} where {$field} {$operator} ?";
				if(!$this->query($sql, array($value))->error()){
					return $this;
				}
			}	
		}
		return false;
	}	
	
	/* CRUD method */
	public function getOne($table, $where){
		return $this->action("SELECT *", $table, $where);	
	}		
	public function delete($table, $where){
		return $this->action("DELETE", $table, $where);				
	}
	public function insert($table, $fields = array()){
		if(count($fields)){
			$keys = array_keys($fields);
			$values = "";
			$i = 1;
			foreach($fields as $field){
				$values .= "?";	
				if($i < count($fields)){
					$values .= ", ";
				}
				$i++;
			}				
			$sql = "INSERT INTO {$table} (`".implode("`, `", $keys)."`) VALUES ({$values})";				
			if(!$this->query($sql, $fields)->error()){
				return true;
			}
		}
		return false; 
	}
	public function update($table, $id_name, $id, $fields){
		$set = "";
		$i = 1;
		foreach($fields as $name => $value){
			$set .= "{$name} = ?";
			if($i < count($fields)){
				$set .= ", ";
			}
			$i++;
		}
		
		$sql = "UPDATE {$table} SET {$set} WHERE {$id_name} = {$id}";
		if(!$this->query($sql, $fields)->error()){
			return true;
		}
		return false;
	}
	
	public function results(){
		return $this->_results;
	}		
	public function first(){
		return $this->_results[0];
	}		
	public function error(){
		return $this->_error;
	}
	public function count(){
		return $this->_count;
	}
}

/* Input check */
class Input{	
	public static function exists($type = "post"){
		switch($type){
			case "post":
				return (!empty($_POST)) ? true : false;
			break;
			case "get":
				return (!empty($_GET)) ? true : false;				
			break;
			default:
				return false;
			break;
		}
	}
	
	public static function get($item){
		if(isset($_POST[$item])){
			return $_POST[$item];
		}
		elseif(isset($_GET[$item])){
			return $_GET[$item];
		}
		return "";
	}
}


/* Session */
class Session{
	public static function exists($name){
		return (isset($_SESSION[$name])) ? true : false;
	}
	
	public static function put($name, $value){
		return $_SESSION[$name] = $value;
	}
	
	public static function get($name){
		return $_SESSION[$name];
	}
	
	public static function delete($name){
		if(self::exists($name)){
			unset($_SESSION[$name]);
		}
	}
	
	public static function flash($name, $string = ""){
		if(self::exists($name)){
			$session = self::get($name);
			self::delete($name);
			return $session;
		}
		else{
			self::put($name, $string);
		}
	}
}

/* Cookie session */
class Cookie{
	public static function exists($name){
		return (isset($_COOKIE[$name])) ? true : false;
	}
	
	public static function get($name){
		return $_COOKIE[$name];
	}
	
	public static function put($name, $value, $expiry){
		if(setcookie($name, $value, time()+$expiry, "/")){
			return true;
		}
		return false;
	}
	
	public static function delete($name){
		self::put($name, "", time()-1);
	}
}

/* Validation */
class Validate{
	private $_passed = false, $_errors = array(), $_db = null;

	public function __construct(){
		$this->_db = DB::getInstance();
	}
	
	public function check($source,  $items = array()){
		foreach($items as $item => $rules){
			foreach($rules as $rule => $rule_value){
				$value = trim($source[$item]);
				$item = escape($item);
				if($rule=="required" and empty($value)){
					$this->addError(ucfirst($item)." is required.");
				}
				elseif(!empty($value)){
					switch($rule){
						case "min":
							if(strlen($value) < $rule_value){
								$this->addError("{$item} must be a minimum of {$rule_value} characters.");
							}
						break;
						case "max":
							if(strlen($value) > $rule_value){
								$this->addError("{$item} must be a maximum of {$rule_value} characters.");
							}
						break;
						case "matches":
							if($value!=$source[$rule_value]){
								$this->addError("{$rule_value} must match{$item}.");
							}							
						break;
						case "unique":
							$check = $this->_db->getOne($rule_value, array($item, "=", $value));
							if($check->count()){
								$this->addError("{$item} already exists.");
							}
						break;
					}
				}					
			}
		}			
		if(empty($this->_errors)){
			$this->_passed = true;
		}
		return $this;
	}
	
	private function addError($error){
		$this->_errors[] = $error;
	}
	public function errors(){
		return $this->_errors;
	}
	public function passed(){
		return $this->_passed;
	}
}

/* Guest subscription */
class Guest{
	private $_db, $_data;
	
	public function __construct(){
		$this->_db = DB::getInstance();
	}
	
	public function create($fields = array()){
		if(!$this->_db->insert("contact", $fields)){
			throw new Exception("An error has been detected in creating the message data.");
		}
	}
	
	public function update($fields = array(), $id_name = null, $id = null){
		if(!$id){
			$id = $this->data()->id;
		}
		if(!$this->_db->update("contact", $id_name, $id, $fields)){
			throw new Exception("An error has been detected in updating the message data.");
		}
	}
	
	public function data(){
		return $this->_data;
	}
}

/* Redirected site */
class Redirect{
	public static function to($location = null){
		if($location){
			if(is_numeric($location)){
				switch($location){
					case "404":
						header("HTTP/1.0 404 Not Found");
						exit();
					break;
				}
			}
			header("Location: ".$location);
			exit();
		}
	}
}

?>