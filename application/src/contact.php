<?php
	/* Initialized variables */
	$contact_title = "get-in-touch";
	$form_title = "contact-form";
	$details_title = "contact-details";
	$sm_title = "social-media";

	$contact_title_layout = array("div"=>"title", "hr"=>"title-divider");
	$col_md8 = "col-md-8 last-col";
	$col_mdx4 = "col-md-4 align-center";
	$contact_form = array("title"=>$col_md12, "contact-form"=>$col_md8, "contact-details"=>$col_mdx4);

	/* Contact form & details */
	$form_class = "contact";
	$form = array(
		"name"=>"text",
		"email"=>"text",
		"subject"=>"text",
		"message"=>"textarea",
		"send"=>"submit"
	);
	$param_submit = "send_message";
	$contact_details = array($details_title=>"contact-info", $sm_title=>"social-media-list");
	$text = "class='form-control'";
	$msg = "class='form-control message'";
	$phone = "+63 909 578 8270<br>+63 975 337 7400";
	$email_add = "jlgnava.blogger@gmail.com";
	$contact_info = array("messenger"=>"jlgnava", "phone"=>$phone, "mail4"=>$email_add);
	$social_media = array(
		"facebook"=>"jlgnava",
		"twitter"=>"jlgnava_blogger",
		"instagram"=>"jlgnava.blogger",
		"tumblr"=>"jlgnava"
	);

	/*
	$col_md12 = "col-md-12 align-center";
	See profile.php
	*/
?>

<section id="contact">
	<div class="overlay-wrap">

    	<div class="container">
	    <section class="row">

			<?php
			$count = 1;
			foreach($contact_form as $attribute=>$colspan){?>

		        <!-- <?php echo capitalizeFirstWord($attribute);?> -->
		        <div class="<?php echo "$colspan contact-form item-$count";?>">
		        <?php
				switch($attribute){

					/* Title */
					case "title":
						foreach($contact_title_layout as $tag=>$type){
							switch($type){
								case "title":
									$contact_top = capitalizeWords($contact_title);
								break;
								case "title-divider": $contact_top = "";
								break;
							}?>
							<<?php echo $tag;?> class="<?php echo $type;?>"><?php echo $contact_top;?></<?php echo $tag;?>>
						<?php
						}
					break;

					/* Contact form */
					case "contact-form":?>
		            <div class="subtitle pad-left"><?php echo capitalizeWords($form_title);?></div>
					<form action="" id="<?php echo $form_class;?>-form" class="form" role="form" method="post">
					<?php
					$group_count = 1;
		            foreach($form as $name=>$input){

						/* Values and classes inside each field */
						if($name=="name"){
							$value = escape(Input::get($name));
							$form_id = str_replace($name, $name, "fullname");
						}
						else{
							$value = "";
							$form_id = $name;
						}

						/* Displaying empty fields */
						switch($name){
							case "email": $keyword = str_replace($name, $name, ucwords("e-mail address"));
							break;
							default: $keyword = capitalizeWords($name);
							break;
						}

						$placeholder = "name='$name' id='$form_id' value='$value' ";
						$placeholder .= "placeholder='$keyword' autocomplete='off'";
						?>

		              	<div class="<?php echo "form-group form-group-$group_count"; ?>">
		               	<?php
		               		switch($input){

								/* Filling out the form */
								case "text":?>
		                    		<input type="<?php echo $input;?>" <?php echo $text.$placeholder;?> onfocus="this.placeholder = ''" onblur="this.placeholder='<?php echo $keyword;?>'"/>
		              			<?php
								break;

								/* Message */
								case "textarea":?>
		                    		<textarea <?php echo $msg.$placeholder;?> onfocus="this.placeholder = ''" onblur="this.placeholder='<?php echo $keyword;?>'"></textarea>
								<?php
								break;

								/* Send a message*/
								case "submit":
									$param_btn = "class='contact-button btn' name='$param_submit' id='submit'";
									$param_value = "value='".ucwords("$keyword message")."'";?>
									<input type="<?php echo $input;?>" <?php echo $param_btn.$param_value;?>/>
			                      	<span class="success-msg"></span>
			                    <?php
								break;
							}?>
		                 	</div>
		              	<?php
						$group_count++;
		              }?>
		       	</form>

				<?php
				break;

				/* Contact details */
				case "contact-details":
					$details_count = 1;
					foreach($contact_details as $title=>$details_class){?>
						<div class="<?php echo "details-layout details-item-$details_count";?>">
						<div class="subtitle pad-left"><?php echo capitalizeWords($title);?></div>
			            <ul class="<?php echo $details_class;?>">
			            <?php
			            switch($title){

			            	/* Contact info*/
			             	case $details_title:
								foreach($contact_info as $icon=>$content){?>
				              		<li class="<?php echo $icon;?>">
										<?php
						                   switch($icon){
						                     case "messenger": $icon_class = "socicon-$icon";
						                     break;
						                     default: $icon_class = "icon-$icon";
						                     break;
						                }?>
					                	<i class="<?php echo $icon_class;?>"></i>
					                	<?php echo $content;?>
					               	</li>
				              		<?php
							}
			                break;

			                /* Social media*/
			                case $sm_title:
								foreach($social_media as $icon=>$username){
									$link = "https://www.$icon.com/$username";
									$user_icon = "socicon-$icon";?>
				                  	<li class="<?php echo $icon;?>">
				                    	<a href="<?php echo $link;?>"><i class="<?php echo $user_icon;?>"></i></a>
				                    </li>
				                <?php
								}
			                break;

			                }?>
			            </ul>
		            </div>
		            <?php
					$details_count++;
				}
				break;
			}?>
	        </div>

	        <?php
			$count++;
			}?>

	    </section>
    	</div>
	</div>
</section>
