<?php
/* Description file*/
include("src/article.php");
	
/* Biography page */
function profileBio(){
	global $topDesc, $middleDesc, $conclusion;
	
	/* Structuring a description */
	$paragraphs = array($topDesc, $middleDesc, $conclusion);
	$description = "";
	foreach($paragraphs as $paragraph_parts){
		$paragraph_layout = "";
		foreach($paragraph_parts as $sentence){
			$paragraph_layout .= $sentence;
		}
		
		$description .= "<div class='desc-section'>$paragraph_layout</div>";
	}
	return $description;
}

/* Close modals */
function closeButton(){
	$rl = "<div class='rl'></div>";
	$lr = "<div class='lr'>$rl</div>";
	$button = "<button type='button' class='close-modal' data-dismiss='modal'>$lr</button>";
	return $button;
}

/* Modal content */
function highlightSetAcad(){
	global $exp_educ;
	$exp_title_layout = "";
	
	$count = 1;
	foreach($exp_educ as $exp_title=>$details){
		
		/* Icon images*/
		$work_count = $count==1 || $count==4;
		if($work_count)
			$icon_img_type = "work";
		else
			$icon_img_type = "educ";
		
		/* Location */
		$exp_title_display = "<h3>$exp_title</h3>";	
		
		/* Occupation & timeframe */
		$exp_content_layout = "";
		$arr_count = 1;
		foreach($details as $exp_content){
			if($arr_count==1)
				$exp_content_layout .= "<h4>$exp_content</h4>";
			else
				$exp_content_layout .= "<div class='date'>$exp_content</div>";
			$arr_count++;
		}
		
		/* Animation directions */
		$fade_left = $count%2;
		if($fade_left)
			$animation = "fadeInLeft";
		else
			$animation = "fadeInRight";
		
		/* Content layout */
		$exp_details = $exp_title_display.$exp_content_layout;
		$icon_img = "<div class='icon-$icon_img_type'></div>";
		$icon = "<div class='icon'>$icon_img</div>";
		$animation_chain = "animation-chain overflow-hidden";
		$animated = "<div class='details $animation_chain' data-animation='$animation'>".$exp_details."</div>";
		$exp_title_layout .= "<li>".$icon.$animated."</li>";
		$count++;
	}
	return $exp_title_layout;
}

/* Modal content */
function highlightMinistryService(){
	global $ministry_list;
	$ms_title_layout = "";
	
	$count = 1;
	foreach($ministry_list as $icon_img_type=>$details){
		
		/* Animation directions */
		$fade_left = $count%2;
		if($fade_left)
			$animation = "fadeInLeft";
		else
			$animation = "fadeInRight";
		
		
		/* Content layout */
		$ms_content_layout = "";
		$arr_count = 1;
		foreach($details as $ms_content){
			
			/* Location */
			if($arr_count==1)
				$ms_content_layout .= "<h3>$ms_content</h3>";
				
			/* Occupation */	
			elseif($arr_count==2)
				$ms_content_layout .= "<h4>$ms_content</h4>";
				
			/* Timeframe */
			else
				$ms_content_layout .= "<div class='date'>$ms_content</div>";
			$arr_count++;
			
		}
		
		$ms_details = $ms_content_layout;
		$icon_img = "<div class='icon-$icon_img_type'></div>";
		$icon = "<div class='icon'>$icon_img</div>";
		$animation_chain = "animation-chain overflow-hidden";
		$animated = "<div class='details $animation_chain' data-animation='$animation'>".$ms_details."</div>";
		$ms_title_layout .= "<li>".$icon.$animated."</li>";
		$count++;
	}
	return $ms_title_layout;
}

function profileFavs(){
	global $profile_set, $favorite_set;
	$prof_fav_set = array("profile"=>$profile_set, "favorites"=>$favorite_set);
	$pf_layout = "";
	
	$count = 1;
	foreach($prof_fav_set as $pf_content_title=>$pf_content){
		
		/* Animation */
		$animation = "fadeInLeft";
		
		/* Title display */
		$pf_title_display = "<h3>".capitalizeWords($pf_content_title)."</h3>";	
		
		/* Content layout */
		$pf_content_list = "";
		foreach($pf_content as $info_label=>$info){
			$pf_content_list .= "<h4>$info_label:<span>$info</span></h4>";
		}
		
		$pf_content_layout = "<div class='modal-content-list'>$pf_content_list</div>";
		$pf_details = $pf_title_display.$pf_content_layout;
		$icon_img = "<div class='icon-jlgnava-$pf_content_title'></div>";
		$icon = "<div class='icon'>$icon_img</div>";
		$animation_chain = "<div class='animation-chain overflow-hidden' data-animation='$animation'>".$pf_details."</div>";
		$pf_layout .= "<li>".$icon.$animation_chain."</li>";
		$count++;
	}
	return $pf_layout;
}

?>