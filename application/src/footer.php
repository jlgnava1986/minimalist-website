<?php
	/* Initialized variables */
	$copy_symbol = "&#169;";
	$copyright = "$copy_symbol 2017 Joel the Explorer. Designed and developed by MantaRaymarc.";

?>
<footer id="footer">
	<div class="container">
    	
        <div class="col-md-12 align-center">
        	<span class="copyright"><?php echo $copyright;?></span>
        </div>
    
    </div>
</footer>