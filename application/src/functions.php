<?php
/* Page Title */
function pageTitle($title, $page){
	$symbol = " | ";
	return $title.$symbol.$page;
}

/* Wordiness */
function firstWord($str){
	$first_word = strtok($str, " ");
	return $first_word;
}

function secondWord($str){
	$first_word = strtok($str, " ");
	$second_word = substr($str, strlen($first_word));
	return $second_word;	
}

function capitalizeFirstWord($str){
	$keyword = preg_replace("/[^a-zA-Z0-9\s]/",  " ", $str);
	$keyword_arr = explode(" ", $keyword);
	$first_item = array_shift($keyword_arr);
	$items = implode(" ", $keyword_arr);
	return ucwords($first_item)." ".$items;
	
}

function capitalizeWords($str){
	$keyword = preg_replace("/[^a-zA-Z0-9\s]/",  " ", $str);
	return ucwords($keyword);
}

function twoWords($str){
	$keyword = preg_replace("/[^a-zA-Z0-9\s]/",  " &amp; ", $str);
	return ucwords($keyword);
}

function multipleWords($str){
	$keyword = preg_replace("/[^a-zA-Z0-9\s]/",  " ", $str);
	$keyword_arr = explode(" ", $keyword);
	$last_item = array_pop($keyword_arr);
	$items = implode(", ", $keyword_arr);
	return ucwords($items .= ", &amp; ".$last_item);
}

/* Facebook URL for the photo album*/
function FacebookPage(){
	$html = "https://www.facebook.com";
	$fb = array($html, "pg", "jlgnava", "photos", "tab=album", "album_id=");
	$fb_url = "";
	
	$count = 1;
	foreach($fb as $syntax){
		if($count > count($fb)-2){
			/* Replaces the slash symbol instead */
			switch($count){
				case count($fb)-1:
					$fb_url .= "?$syntax&";
				break;
				case count($fb):
					$fb_url .= "$syntax";
				break;
			}
		}
		else
			/* Adds slash after the word*/
			$fb_url .= "$syntax/";
		$count++;
	}
	
	return $fb_url;
}?>