<?php
	$tableCell = "class='td verticalmiddle'";
	$logo = "id='logo'";
	$topNav = array($logo, "");
	$gradient = array("light", "dark");
	$sidebar = "class='navbar-toggle' data-toggle='collapse' data-target='.nav-show-menu'";
	$navbar = array(
		"home"=>"nav", 
		"about"=>"nav-about", 
		"highlights"=>"nav-high", 
		"photos"=>"nav-photos", 
		"contact"=>"nav-contact"
	);
	$topSection = array("page-title", "page-captions", "scroll-down");
	$topLink = "#top-section";
	$title = "joel-the-explorer";
	$profile = array("personal-blogger", "photo-journalist", "online-missionary-of-god");
	$scroll_down = "find-out-more";
?>

<!-- Navigational menu -->
<nav class="navbar navbar-fixed-top align-center navbar-header" role="navigation">
    <div class="container">
    	
        <!-- Table format -->
        <div class="table">
        	<div class="tr">
			<?php
            foreach($topNav as $topFeature){?>
                <div <?php echo "$tableCell $topFeature";?>>
                <?php
                switch($topFeature){
                    case $logo:?>
                        <!-- Logo -->
                        <a href="<?php echo $topLink;?>">
						<?php 
                        foreach($gradient as $gradtype){?>
                            <img src="img/jlgnava-<?php echo $gradtype;?>.png" class="<?php echo $gradtype;?>-logo"/>
                        <?php
                        }?>
						</a>
                    <?php
                    break;
                    default:?>
                    
                    	<!-- Top nav menu -->
                        <div class="navbar-collapse nav-show-menu collapse">
                            <ul class="nav nav-top">
                            <?php
							$count = 1;
							foreach($navbar as $tabs=>$hover){
								switch($tabs){
									case "home": $href = "href='#top-section'";
									break;
									case "about": $href = "href='#".str_replace($tabs, $tabs, "profile")."'"; 
									break;
									case "photos": $href = "href='#".str_replace($tabs, $tabs, "photo-gallery")."'";
									break;
									default: $href = "href='#$tabs'";
									break;
								}?>
                                <li>
                                    <a class="transition <?php echo $hover;?>" <?php echo $href;?>>
                                    	<?php echo capitalizeWords($tabs);?>
                                    </a>
                                </li>
                           		<?php
								$count++;
							}?>
                           	</ul>
                        </div>
                        
                       <!-- Menu button (for mobile devices only) -->
                    	<button type="button" <?php echo $sidebar;?>>
                        	<div class="hamburger"></div>
                        </button>
                        
                    <?php
                    break;
                }?>
                </div>
            <?php	
            }?>
        	</div>
        </div>
    
    </div>
</nav>

<!-- Main page -->
<div id="top-section" class="nav-bar parallax-bg align-center">
	<div class="overlay-wrap">
    
    	<header>
        	<div class="container">
            
            	<section class="row">
                	<?php
					$count = 1;
					foreach($topSection as $class){?>
                    	<div class="<?php echo "$class front-cover top-item-$count";?>">
                        <?php
						switch($class){
							case "page-title": echo capitalizeWords($title);
							break;
							case "page-captions":
								$captions_count = 1;
								foreach($profile as $attribute){?>
                                	<span class="<?php echo "caption-attribute-$captions_count";?>">
										<?php echo capitalizeWords($attribute);?>
                                   	</span>
                                	<?php
									$captions_count++;
								}
							break;
							case "scroll-down":?>
                            	<a href="#profile" class="scroll-down-btn btn"><?php echo capitalizeWords($scroll_down);?></a>
							<?php
							break;
						}?>
                        </div>
						<?php	
						$count++;
					}?>
                </section>
            
            </div>
        </header>
    
    </div>
</div>