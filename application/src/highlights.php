<?php
	/* Initialized variables */
	$highlights_title = "the explorer's notebook";
	$hl_title_layout = array("div"=>"title", "hr"=>"title-divider");
	$col_md4 = "col-md-4";
	$hl_layout = array("title"=>$col_md12, "highlight-set"=>$col_md12, "next-section"=>$col_md12);
	$highlight_set = array(
		"profile"=>"profile-and-favorites",
		"acad"=>"experience-and-education", 
		"christ"=>"ministry-services"
	);
	$highlight_set_each = array("highlight-title", "highlight-button");
	$highlight_button = "view-more";
	$scroll_btn = "nextbutton nobg scroll-animate";
	$nxtsection = "#photo-gallery";
	
	/* 
	$col_md12 = "col-md-12 align-center"; 
	See profile.php 
	*/
	
?>

<section id="highlights">
	<div class="overlay-wrap">
    	<div class="container">
        	
            <section class="row">
            	
            <?php
			$count = 1;
			foreach($hl_layout as $layout=>$colspan){?>
				
				<!-- <?php echo capitalizeFirstWord($layout);?> -->
                <div class="<?php echo "$colspan highlights-layout item-$count";?>">
                <?php
				switch($layout){
					
					/* Title */
					case "title":
						foreach($hl_title_layout as $tag=>$type){
							switch($type){
								case "title": 
								$highlights_top = ucwords($highlights_title);
								break;
								case "title-divider": $highlights_top = "";
								break;
							}?>
							<<?php echo $tag;?> class="<?php echo $type;?>"><?php echo $highlights_top;?></<?php echo $tag;?>>
						<?php
						}
					break;
					
					/* Highlight set */
					case "highlight-set":?>
                    	<ul class="<?php echo $layout;?>">
                        <?php
						$set_count = 1;
						foreach($highlight_set as $class=>$title){?>
							
                            <!-- <?php echo capitalizeFirstWord($title);?> -->
                            <li class="<?php echo "$col_md4 $class $layout-item highlight-set-$set_count"; ?>">
                            	
								<?php
								foreach($highlight_set_each as $type){?>
									<div class="<?php echo $type;?>">
                                   
                                    <?php
									switch($type){
										case "highlight-title": 
											if($class=="profile")
												echo str_replace($title, $title, "Profile & favorites");
											elseif($class=="acad")
												echo str_replace($title, $title, "Experience & education");
											else
												echo capitalizeWords($title);
										break;
										case "highlight-button":
											$modal_effect = "href='#highlight-set-$class' data-toggle='modal'";?>
                                        	<a class="btn highlight-open-modal" <?php echo $modal_effect;?>>
                                           		<?php echo capitalizeWords($highlight_button);?>
                                            </a>
										<?php
										break;
									}?>
                                    
                                    </div>
								<?php		
								}?>
                                
                            </li>
							<?php
							$set_count++;
                        }?>
                        </ul>
					<?php						
					break;
					
					/* Next section */
					case "next-section":?>
                        <div class="scroll-down">
                            <a href="<?php echo $nxtsection;?>" class="<?php echo $scroll_btn;?>"><span></span></a>
                        </div>
					<?php
                    break;
				}?>
				</div>
				<?php
            	$count++;
			}?>    
                
            </section>
        </div>
    
    </div>
</section>    

