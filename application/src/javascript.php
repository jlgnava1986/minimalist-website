<!-- Javascript Files -->
<?php
	$js = array("modernizer", "jquery.min", "bootstrap.min", "jquery.easing", "validator.min", "bsvalid", "custom");
	foreach($js as $jsfile){?>
		<script type="text/javascript" src="js/<?php echo "$jsfile.js";?>"></script>
    <?php	
	}
?>

<!-- Input Form -->
<script type="text/javascript">
	$(document).ready(function(){
	
		if(!Modernizr.input.placeholder){
		
			$('[placeholder]').focus(function() {
			  var input = $(this);
			  if (input.val() == input.attr('placeholder')) {
				input.val('');
				input.removeClass('placeholder');
			  }
			}).blur(function() {
			  var input = $(this);
			  if (input.val() == '' || input.val() == input.attr('placeholder')) {
				input.addClass('placeholder');
				input.val(input.attr('placeholder'));
			  }
			}).blur();
			$('[placeholder]').parents('form').submit(function() {
			  $(this).find('[placeholder]').each(function() {
				var input = $(this);
				if (input.val() == input.attr('placeholder')) {
				  input.val('');
				}
			  })
			});
		
		}
	});
	
	if(!Modernizr.svg){
		// Get all img tag of the document and create variables
		var i=document.getElementsByTagName("img"),j,y;
	
		// For each img tag
		for(j = i.length ; j-- ; ){
			y = i[j].src
			// If filenames ends with SVG
			if( y.match(/svg$/) ){
				// Replace "svg" by "png"
				i[j].src = y.slice(0,-3) + 'png'
			}
		}
	}
	
</script>