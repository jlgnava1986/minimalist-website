<!-- Highlight modals -->
<?php
	/* Initialized variables*/
	$modal_layout = array("modal-title"=>$col_md12, "modal-content-layout"=>$col_md12);
	$modal_title_layout = array("div"=>"title", "hr"=>"title-divider");

	/* Modal layout*/
	foreach($highlight_set as $highlight_attr=>$highlight_title){
		$highlight_class = "id='highlight-set-$highlight_attr' class='highlight-modal modal fade'";?>
		<div <?php echo "$highlight_class";?> role="dialog" tabindex=-1 aria-hidden="true">
	
			<div class="modal-content">
				
				<section class="row">
                <?php
                $count = 1;
				foreach($modal_layout as $modal_class=>$colspan){?>
                
                	<!-- <?php echo capitalizeFirstWord($modal_class);?> -->
					<div class="<?php echo "modal-layout $colspan modal-item-$count";?>">
                    <?php
                    switch($modal_class){
						
						/* Title and close button*/
						case "modal-title":
                        	foreach($modal_title_layout as $tag=>$type){
								switch($type){
									case "title": 
										
										switch($highlight_attr){
											case "christ":
												$modal_top = capitalizeWords($highlight_title);
											break;
											default:
												if($highlight_attr=="profile")
													$replace_title = "Profile & favorites";
												if($highlight_attr=="acad")
													$replace_title = "Experience & education";
												$modal_top = str_replace($highlight_title, $highlight_title, $replace_title);
											break;
										}
										
									break;
									case "title-divider": $modal_top = "";
									break;
								}?>
								<<?php echo $tag;?> class="<?php echo $type;?>"><?php echo $modal_top;?></<?php echo $tag;?>>
							<?php
							}
							echo closeButton();
						break;
						
						case "modal-content-layout":?>
                        	<ul class="timeline">
                            <?php
							switch($highlight_attr){
								
								case "profile":
									echo profileFavs();
								break;
								
								case "acad":
									echo highlightSetAcad();
								break;
								
								case "christ":
									echo highlightMinistryService();
								break;
								
							}?>
							</ul>
                        <?php
						break;
						  
					}?> 
                    </div>
                    <?php
					$count++;
				}?>
                </section>
					
			</div>
			
		</div>
	<?php
	}
?>

