<?php
	/* Initialized variables */
	$gallery_title = "the explorer's gallery";
	$photo_title_layout = array("div"=>"title", "hr"=>"title-divider");
	$gallery_layout = array("title"=>$col_md12, "photo-gallery"=>$col_md12, "next-section"=>$col_md12);
	$gallery_set = array(
		"portfolio"=>"personal-portfolio",  
		"prayer"=>"prayer-meetings", 
		"gigs"=>"random gigs"
	);
	$gallery_set_each = array("gallery-title", "gallery-button");
	$gallery_button = "view-photos";
	$nxtsection = "#contact";
	
	/* 
	$col_md4 = "col-md-4";
	$col_md12 = "col-md-12 align-center";
	See profile.php
	*/

?>
<section id="photo-gallery">
	<div class="gallery-content">
    
    	<div class="container">
        	<section class="row">
            
            <?php
			$count = 1;
			foreach($gallery_layout as $layout=>$colspan){?>
            
            	<!-- <?php echo capitalizeFirstWord($layout);?> -->
                <div class="<?php echo "$colspan photo-gallery-layout item-$count";?>">
                <?php
				switch($layout){
					
					/* Title */
					case "title":
						foreach($photo_title_layout as $tag=>$type){
							switch($type){
								case "title": 
								$gallery_top = ucwords($gallery_title);
								break;
								case "title-divider": $gallery_top = "";
								break;
							}?>
							<<?php echo $tag;?> class="<?php echo $type;?>"><?php echo $gallery_top;?></<?php echo $tag;?>>
						<?php
						}
					break;
					
					/* Gallery set */
					case "photo-gallery":?>
                    	<ul class="<?php echo $layout;?>">
                        <?php
						$set_count = 1;
						foreach($gallery_set as $class=>$title){?>
                        	
                            <!-- <?php echo capitalizeFirstWord($title);?> -->
                            <li class="<?php echo "$col_md4 $class $layout-item $layout-$set_count"; ?>">
                            
                            	<?php
								foreach($gallery_set_each as $type){?>
									<div class="<?php echo $type;?>">
                                   
                                    <?php
									switch($type){
										case "gallery-title": echo ucwords(capitalizeWords($title));
										break;
										case "gallery-button":?>
                                        	<a class="btn" href="<?php echo "?page=$class";?>" data-toggle="modal">
                                           		<?php echo capitalizeWords($gallery_button);?>
                                            </a>
										<?php
										break;
									}?>
                                    
                                    </div>
								<?php		
								}?>
                            
                            </li>
							<?php
							$set_count++;
                        }?>
                        </ul>
					<?php
					break;
					
					/* Next section */
					case "next-section":?>
                        <div class="scroll-down">
                            <a href="<?php echo $nxtsection;?>" class="<?php echo $scroll_btn;?>"><span></span></a>
                        </div>
					<?php
                    break;
					
				}?>
                </div>
                
            	<?php
				$count++;
			}?>
            
            </section>
        </div>    

	</div>
</section>