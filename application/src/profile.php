<?php
	/* Initialized variables */
	$profile_title = "about-the-explorer";
	$profile_headshot = "headshot.jpg";
	$profile_name = "jose-luis-garrucho-nava";
	$profile_title_layout = array("div"=>"title", "hr"=>"title-divider");
	$col_md12 = "col-md-12 align-center";
	$col_md6 = "col-md-6 align-center";
	$col_mdx6 = "col-md-6 last-col";
	$profile_layout = array(
		"title"=>$col_md12, 
		"image-headshot"=>$col_md6, 
		"profile-bio"=>$col_mdx6, 
		"next-section"=>$col_md12
	);
	$profile_content = array("subtitle", "desc");
	$scroll_btn = "nextbutton nobg scroll-animate";
	$nxtsection = "#highlights";
?>

<section id="profile">
	<div class="profile-content">
    
    	<div class="container">
        	<section class="row">
            
			<?php
			$count = 1;
			foreach($profile_layout as $layout=>$colspan){?>
            
            	<!-- <?php echo capitalizeFirstWord($layout);?> -->
            	<div class="<?php echo "$colspan profile-layout item-$count";?>">
                <?php
				switch($layout){
					
					/* Title */
					case "title":
						foreach($profile_title_layout as $tag=>$type){
							switch($type){
								case "title": 
								$profile_top = capitalizeWords($profile_title);
								break;
								case "title-divider": $profile_top = "";
								break;
							}?>
							<<?php echo $tag;?> class="<?php echo $type;?>"><?php echo $profile_top;?></<?php echo $tag;?>>
						<?php
						}
					break;
					
					/* Image headshot */
					case "image-headshot":?>
                        <div class="headshot"><img src="<?php echo "img/jlgnava-$profile_headshot";?>"/></div>
                    <?php
                    break;
					
					/* Biography */
					case "profile-bio":
						foreach($profile_content as $type){
							switch($type){
								case "subtitle": $profileBio = capitalizeWords($profile_name);
								break;
								case "desc": $profileBio = profileBio();
								break;
							}?>
							<!-- Profile details -->
							<div class="<?php echo "$type"?>"><?php echo $profileBio; ?></div>
						<?php
						}
					break;
					
					/* Next section */
					case "next-section":?>
                        <div class="scroll-down">
                            <a href="<?php echo $nxtsection;?>" class="<?php echo $scroll_btn;?>"><span></span></a>
                        </div>
					<?php
                    break;
				}?>
                </div>
                
                <?php
				$count++;
			}?>
            
        </section>
    
    </div>
</section>